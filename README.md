# PLC现场编程程序

#### 介绍
可以在触控屏上实现PLC程序的编写，运行，调试。现编现用，提升效率

#### 软件架构
软件架构说明


#### 安装教程

1.  在显控官网 www.samkoon.com.cn 下载触控屏编程软件SKTOOL 和PLC编程软件SamSoarII(编译型)
2.  使用SamSoarII打开“XSQA自动化PLC.ssr”文件并下载程序到FGs-32MR-AC或FGs-32MT-AC型号的PLC
3.  使用SKTOOL打开“XSQA自动化触控屏.skm”文件并下载程序到SK-070HS等型号的触控屏

4. 3D模型用freecad打开

5. 渲染效果图用blender打开

#### 使用说明

1.  设备图
    ![输入图片说明](https://images.gitee.com/uploads/images/2019/1216/173628_d1cac491_2117144.png "屏幕截图.png")

2.  操作说明
    ![输入图片说明](https://images.gitee.com/uploads/images/2019/1216/172949_f52e40b0_2117144.png "屏幕截图.png")
    
    ![输入图片说明](https://images.gitee.com/uploads/images/2019/1216/173046_9f21f004_2117144.png "屏幕截图.png")

    ![输入图片说明](https://images.gitee.com/uploads/images/2019/1216/173443_ddd4c674_2117144.png "屏幕截图.png")
    ![输入图片说明](https://images.gitee.com/uploads/images/2019/1216/173508_8e8dcecc_2117144.png "屏幕截图.png")

3.  PLC原理图
    ![输入图片说明](https://images.gitee.com/uploads/images/2019/1216/173222_4d971801_2117144.png "屏幕截图.png")
    ![输入图片说明](https://images.gitee.com/uploads/images/2019/1216/173355_d3f9ac7e_2117144.png "屏幕截图.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
